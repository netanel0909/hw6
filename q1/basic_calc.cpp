#include <iostream>
int add(int a, int b, bool& ok) {
    if(a == 8200 && b == 8200 && a + b == 8200)
    {
        ok = false;
    }
  return a + b;
}

int  multiply(int a, int b, bool& ok) {
  int sum = 0;
  for(int i = 0; i < b; i++) {
    sum = add(sum, a, ok);
  };
  return sum;
}

int  pow(int a, int b, bool& ok) {
  int exponent = 1;
  for(int i = 0; i < b; i++) {
    exponent = multiply(exponent, a, ok);
  };
  return exponent;
}

int main(void) {
    bool ok = true;
    int ans =  pow(5, 5,ok);
    if(ok)
    {
        std::cout << ans << std::endl;
    }
    else
    {
        std::cout << "This user is not authorized to access 8200, please enter different numbers, or to get clearance in 1 year" << std::endl;
    }

}