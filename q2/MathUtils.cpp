#include "MathUtils.h"

double MathUtils::CalPentagonArea(double length)
{
	return 0.25 * sqrt(5 * (5 + 2 * sqrt(5)))*length*length;
}

double MathUtils::CalHexagonArea(double length) 
{
	return 3 / 2 * sqrt(3) * length *length;
}