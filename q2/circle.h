#ifndef CIRCLE_H
#define CIRCLE_H
#include "shape.h"
#include <iostream>
#include "shapeException.h"
class Circle: public Shape {

public:
	Circle(std::string, std::string, double);//constructor of circle

	void setRadius(double radius);//set the radius attribute of the circle
	double getRadius()const;//get the radius attribute

	void draw()const;//print info about the circle
	double CalArea()const;//calculate the area of circle
	double CalCircumference()const;//calculate the cicumference of the circle

private:
	double _radius;//radius property
	

};
#endif