
#include "shape.h"
#include "circle.h"
#include <iostream>
#ifndef quadrilateral_H
#define quadrilateral_H

class quadrilateral :public Shape {
public:
	
	quadrilateral(std::string, std::string, int, int);//ctor
	double getCalPerimater()const;//get
	void setHeight(int h);//set
	void setWidth(int w);
	int getHeight()const;//get
	int getWidth()const;

	void draw()const;//draw func
	double CalArea()const;//calcarea of quadrilateral
	double CalPerimater()const;//calc perimater

private:
	int _width;
	int _height;

};
#endif