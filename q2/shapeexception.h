#pragma once
#include <exception>

class shapeException : public std::exception
{
	virtual char const * what() const
	{
		return "This is a shape exception!";
	}
};