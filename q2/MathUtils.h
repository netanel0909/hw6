#ifndef MATHUTILS_H
#define MATHUTILS_H

#include <math.h>
class MathUtils 
{
public:
	static double CalPentagonArea(double);//Calc area of the pentagon with double param
	static double CalHexagonArea(double); //Calc area of the hexagon with double param

};
#endif