
#include "parallelogram.h"
#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "shapeException.h"
#include <iostream>


parallelogram::parallelogram(std::string col, std::string nam, int h, int w, double ang, double ang2):quadrilateral(col, nam, h, w) {
	 setAngle(ang, ang2);
}
void parallelogram::setAngle(double ang, double ang2) {
	if (std::cin.fail())
	{
		throw InputException();
	}
	else if ((ang < 0 || ang>180) || (ang2 < 0 || ang2>180))
	{
		throw shapeException();
	}
	this->_angle = ang;
	this->_angle2 = ang2;
}
double parallelogram::getAngle()const {
	return this->_angle;
}
double parallelogram::getAngle2() const {
	return this->_angle2;
}


void parallelogram::draw()const
{
	std::cout <<getName()<< std::endl
		<< getColor() << std::endl
		<< "Height is " << getHeight() << std::endl
		<< "Width is " << getWidth() << std::endl
		<< "Angles are: " << getAngle()<<","<<getAngle2()<< std::endl
		<<"Area is "<<CalArea(getWidth(),getHeight())<< std::endl;
}

double parallelogram::CalArea(double w, double h) const{
	if (w < 0 || h < 0)
	{
		throw shapeException();
	}
	return w*h;
}

