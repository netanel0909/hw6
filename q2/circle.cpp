
#include "circle.h"

Circle::Circle(std::string nam, std::string col, double rad):Shape(col, nam) {
	if (rad < 0)
	{
		throw shapeException();
	}
	setRadius(rad);		
}
void Circle::draw() const
{
	std::cout << std::endl<<"Color is "<<getColor() << std::endl
		<<"Name is "<< getName()<< std::endl
		<<"radius is "<<this->getRadius()<< std::endl
		<<"Circumference: "<< CalCircumference()<< std::endl;
}

void Circle::setRadius(double rad){
	if (rad < 0)
	{
		throw shapeException();
	}
	else if (std::cin.fail()) 
	{
		throw InputException();
	}
	this->_radius = rad;
}

double Circle::CalArea()const {
	double area = 3.14*this->_radius*this->_radius;
	return area;
}

double Circle:: getRadius()const {
	return this->_radius;
}


double Circle::CalCircumference() const{
	double circumference = 2 * (3.14)*this->_radius;
	return circumference;
}