#include "Hexagon.h"

Hexagon::Hexagon(std::string name, std::string col,double length):Shape(col,name)
{
	if (length < 0) 
	{
		throw shapeException();
	}
	setLength(length);
}
double Hexagon::getLength()const
{
	return this->_length;
}

void Hexagon::setLength(double length)
{
	if (length < 0)
	{
		throw shapeException();
	}
	else if (std::cin.fail())
	{
		throw InputException();
	}
	this->_length = length;
}


void Hexagon::draw() const
{
	std::cout << std::endl << "Color is " << getColor() << std::endl 
		<< "Name is " << getName() << std::endl
		<< "length is " << this->getLength() << std::endl 
		<< "Area: " << MathUtils::CalHexagonArea(this->_length) << std::endl;
}
