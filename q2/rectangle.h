#ifndef RECTANGLE_H
#define RECTANGLE_H
#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include <iostream>

class rectangle : public quadrilateral {
public:
	rectangle(std::string, std::string, int, int);//ctor

	void draw()const;//draw func
	double CalArea(double, double)const;//calc area rectangle
	bool isSquare(int, int)const;//check if squere


};
#endif