#ifndef Shape_H
#define Shape_H
#include<string>
#include <iostream>
#include "InputException.h"
class Shape
{

public:
	Shape(std::string, std::string);//ctor
	
	std::string getName()const;//get
	std::string getColor()const;
	void setName(std::string);//set
	void setColor(std::string);

	virtual void draw()const = 0; //draw func
	virtual double CalArea()const;//calc func
	static int getCount();//count stat num
	~Shape();
private:
	static int _count;
	std::string _name;
	std::string _color;
	
};

#endif