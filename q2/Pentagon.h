#ifndef PENTAGON_H
#define PENTAGON_H
#include <string>
#include "shape.h"
#include "shapeexception.h"
#include "MathUtils.h"
class Pentagon :public Shape
{
public:

	Pentagon(std::string name, std::string col, double length);//ctor
	double getLength()const;//return length
	void setLength(double);//set
	void draw() const;
private:
	double _length;
};
#endif