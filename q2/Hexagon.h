#ifndef HEXAGON_H
#define HEXAGON_H
#include "shape.h"
#include "shapeexception.h"
#include "MathUtils.h"
class Hexagon : public Shape
{
public:
	Hexagon(std::string, std::string,double);//ctor
	void setLength(double length);//set
	double getLength()const;//get
	void draw() const;//draw func

private:
	double _length;
};
#endif
