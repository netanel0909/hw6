#ifndef INPUTEXCEPTION_H
#define INPUTEXCEPTION_H

#include <exception>

class InputException : public std::exception
{
	virtual char const* what() const
	{
		if (std::cin.fail())//check flag
		{
			std::cin.clear();
			std::cin.ignore(std::numeric_limits < std::streamsize >::max(), '\n');
			return "This is an input exception!";
		}
		return NULL;

	}
};
#endif