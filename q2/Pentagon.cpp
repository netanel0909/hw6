#include "Pentagon.h"

Pentagon::Pentagon(std::string name, std::string col, double length) :Shape(col, name)
{
	if (length < 0)
	{
		throw shapeException();
	}
	setLength(length);
}
void Pentagon::draw() const
{
	std::cout << std::endl << "Color is " << getColor() << std::endl << "Name is " << getName() << std::endl << "length is " << this->getLength() << std::endl << "Area: " << MathUtils::CalPentagonArea(this->_length) << std::endl;
}

void Pentagon::setLength(double length)
{
	if (length < 0)
	{
		throw shapeException();
	}
	else if (std::cin.fail())
	{
		throw InputException();
	}
	this->_length = length;
}

double Pentagon::getLength()const 
{
	return this->_length;
}
