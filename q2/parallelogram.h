#pragma once
#ifndef PARALLELOGRAM_H
#define PARALLELOGRAM_H
#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include <iostream>

class parallelogram : public quadrilateral {

public:
	parallelogram(std::string col, std::string nam, int h, int w, double ang, double ang2);//ctor

		void setAngle(double, double);//set angles
		double getAngle2()const;//get
		double getAngle()const;

		void draw()const;//draw func
		double CalArea(double w, double h)const;//calc area of the parallelogram
private:
	double _angle;
	double _angle2;
};
#endif;