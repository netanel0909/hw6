#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "shapeException.h"
#include <iostream>

quadrilateral::quadrilateral(std::string nam, std::string col, int h, int w):Shape(nam, col) {

	setHeight( h);
	 setWidth( w);
	
}
int quadrilateral::getHeight()const {
	return this->_height;
}
int quadrilateral::getWidth()const {
	return this->_width;
}
void quadrilateral::setHeight(int h) {
	if (std::cin.fail())
	{
		throw InputException();
	}
	this->_height = h;
}
void quadrilateral::setWidth(int w) {
	if (std::cin.fail())
	{
		throw InputException();
	}
	this->_width = w;
}

void quadrilateral::draw()const
{
	std::cout << getName()<< std::endl << getColor() << std::endl<< "Width is " << getWidth() << std::endl << "Height is " << getHeight() << std::endl <<"Area is "<<CalArea()<< std::endl<<"Perimeter is "<<getCalPerimater()<< std::endl;
}

double quadrilateral::CalArea()const
{
	if (this->_width < 0 || this->_height < 0)
	{
		throw shapeException();
	}
	return this->_width*this->_height;
}

double quadrilateral::CalPerimater() const{
	if (this->_width < 0 ||this->_height < 0)
	{
		throw shapeException();
	}
	return 2 * (this->_height + this->_width);
}
double quadrilateral::getCalPerimater()const {
	if (this->_width < 0 || this->_height< 0)
	{
		throw shapeException();
	}
	return 2 * (this->_height + this->_width);

}
