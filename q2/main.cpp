#include <iostream>
#include <string>
#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "rectangle.h"
#include "parallelogram.h"
#include "shapeException.h"
#include "InputException.h"
#include "MathUtils.h"
#include "Pentagon.h"
#include "Hexagon.h"

int main()
{
	std::string nam, col;
	double rad = 0, ang = 0, ang2 = 180,length=0;
	int height = 0, width = 0;

	Circle circ(col, nam, rad);
	quadrilateral quad(nam, col, width, height);
	rectangle rec(nam, col, width, height);
	parallelogram para(nam, col, width, height, ang, ang2);
	Pentagon pent(nam, col, length);
	Hexagon hexagon(nam, col, length);

	Shape *ptrcirc = &circ;
	Shape *ptrquad = &quad;
	Shape *ptrrec = &rec;
	Shape *ptrpara = &para;

	Shape *ptrhexog = &hexagon;
	Shape *ptrpentagon= &pent;


	std::cout << "Enter information for your objects" << std::endl;
	const char circle = 'c', quadrilateral = 'q', rectangle = 'r', parallelogram = 'p',hex = 'h',pentagon ='e'; //with new ones
	
	char shapetype;
	char x = 'y';
	while (x != 'x')
	{
		std::cout << "which shape would you like to work with?.. \nc=circle, q = quadrilateral, r = rectangle, p = parallelogram, pentagon = e,hexagon= h" << std::endl;
		std::cin >> shapetype;
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		try
		{
			switch (shapetype)
			{
			case 'c':
				std::cout << "enter color, name,  rad for circle" << std::endl;
				std::cin.clear();
				std::cin >> col >> nam >> rad;
				circ.setColor(col);
				circ.setName(nam);
				circ.setRadius(rad);
				ptrcirc->draw();
				break;
			case 'q':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin.clear();			
				std::cin >> nam >> col >> height >> width;
				quad.setName(nam);
				quad.setColor(col);
				quad.setHeight(height);
				quad.setWidth(width);
				ptrquad->draw();
				break;
			case 'r':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin.clear();
				std::cin >> nam >> col >> height >> width;
				rec.setName(nam);
				rec.setColor(col);
				rec.setHeight(height);
				rec.setWidth(width);
				ptrrec->draw();
				break;
			case 'p':
				std::cout << "enter name, color, height, width, 2 angles" << std::endl;
				std::cin.clear();
				std::cin >> nam >> col >> height >> width >> ang >> ang2;
				para.setName(nam);
				para.setColor(col);
				para.setHeight(height);
				para.setWidth(width);
				para.setAngle(ang, ang2);
				ptrpara->draw();
				break;
			case 'e':
				std::cout << "enter color, name,  length for pentagon" << std::endl;
				std::cin.clear();
				std::cin >> col >> nam >> length;
				pent.setName(nam);
				pent.setColor(col);
				pent.setLength(length);
				ptrpentagon->draw();
				break;
			case 'h':
				std::cout << "enter color, name,  length for hexagon" << std::endl;
				std::cin.clear();
				std::cin >> col >> nam >> length;
				hexagon.setName(nam);
				hexagon.setColor(col);
				hexagon.setLength(length);
				ptrhexog->draw();
				break;
			default:
				std::cout << "you have entered an invalid letter, please re-enter" << std::endl;
				break;
			}
			std::cout << "would you like to add more object press any key if not press x" << std::endl;
			std::cin.get() >> x;
		}
		catch (std::exception &e)
		{
			printf(e.what());
		}
		catch (...)
		{
			printf("caught a bad exception. continuing as usual\n");
		}
	}



	system("pause");
	return 0;

}